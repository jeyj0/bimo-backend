package com.gitlab.jeyj0.bimobackend.domain.children;

import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRegistry;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRepository;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.created.CreatedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted.DeletedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated.UpdatedEventFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
public class ChildMutationIntegrationTestConfig {

    @Bean
    public ChildMutation childMutation() {
        return new ChildMutation();
    }

    @Bean
    public ChildRepository childRepository() {
        return new ChildRepository();
    }

    @Bean
    public GenericEventSourcingBasedRepository<Child> genericEventSourcingBasedRepository() {
        return new GenericEventSourcingBasedRepository<>();
    }

    @Bean
    public EventRegistry eventRegistry() {
        return new EventRegistry();
    }

    @Bean
    public EventRepository eventRepository() {
        return mock(EventRepository.class);
    }

    @Bean
    public EventFactory eventFactory() {
        return new EventFactory();
    }

    @Bean
    public CreatedEventFactory createdEventFactory() {
        return new CreatedEventFactory();
    }

    @Bean
    public UpdatedEventFactory updatedEventFactory() {
        return new UpdatedEventFactory();
    }

    @Bean
    public DeletedEventFactory deletedEventFactory() {
        return new DeletedEventFactory();
    }

    @Bean
    public ChildFactory childFactory() {
        return new ChildFactory();
    }

    @Bean
    public ChildObjectToStringConverter childObjectToStringConverter() {
        return new ChildObjectToStringConverter();
    }
}

package com.gitlab.jeyj0.bimobackend.domain.children;

import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StringToChildObjectConverterTest {

    private static final String CHILD_ID = "childId";
    private static final String CHILD_NAME = "childName";
    private static final String STATUS_ID_1 = "statusId1";
    private static final String STATUS_ID_2 = "statusId2";
    private static final String CHILD_AS_STRING = "{\"id\":\"" + CHILD_ID + "\",\"name\":\"" + CHILD_NAME + "\",\"statusIds\":[\"" + STATUS_ID_1 + "\",\"" + STATUS_ID_2 + "\"]}";

    @InjectMocks
    private StringToChildObjectConverter stringToChildObjectConverter;

    @Mock
    private ChildFactory childFactory;

    @Test
    public void shouldConvertStringToChild() throws IOException {
        // given
        when(childFactory.createChild(eq(CHILD_ID), eq(CHILD_NAME), anySet())).thenReturn(givenChild());

        // when
        Child child = stringToChildObjectConverter.convertStringToObject(CHILD_AS_STRING);

        // then
        assertThat(child, is(givenChild()));
    }

    private Child givenChild() {
        return new ChildFactory().createChild(CHILD_ID, CHILD_NAME, givenStatusIds());
    }

    private Set<String> givenStatusIds() {
        Set<String> statusIds = Sets.newHashSet();
        statusIds.add(STATUS_ID_1);
        statusIds.add(STATUS_ID_2);
        return statusIds;
    }
}
package com.gitlab.jeyj0.bimobackend.domain.children;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Sets;

@RunWith(MockitoJUnitRunner.class)
public class ChildObjectStringConverterTest {

    private static final String CHILD_ID = "childId";
    private static final String CHILD_NAME = "childName";
    private static final String STATUS_ID_1 = "statusId1";
    private static final String STATUS_ID_2 = "statusId2";
    private static final String CHILD_AS_STRING = "{\"id\":\"" + CHILD_ID + "\",\"name\":\"" + CHILD_NAME + "\",\"statusIds\":[\"" + STATUS_ID_1 + "\",\"" + STATUS_ID_2 + "\"]}";

    @InjectMocks
    private ChildObjectToStringConverter childToObjectStringConverter;

    @Test
    public void shouldConvertChildToString() throws JsonProcessingException {
        // given
        Child child = givenChild();

        // when
        String childAsJSON = childToObjectStringConverter.convertObjectToString(child);

        // then
        assertThat(childAsJSON, is(CHILD_AS_STRING));
    }

    private Child givenChild() {
        return new ChildFactory().createChild(CHILD_ID, CHILD_NAME, givenStatusIds());
    }

    private Set<String> givenStatusIds() {
        Set<String> statusIds = Sets.newHashSet();
        statusIds.add(STATUS_ID_1);
        statusIds.add(STATUS_ID_2);
        return statusIds;
    }

}
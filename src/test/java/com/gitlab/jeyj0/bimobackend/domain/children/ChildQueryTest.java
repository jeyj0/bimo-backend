package com.gitlab.jeyj0.bimobackend.domain.children;

import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChildQueryTest {

    @InjectMocks
    private ChildQuery childQuery;

    @Mock
    private ChildRepository childRepository;

    private ChildFactory childFactory = new ChildFactory();

    @Test
    public void shouldGetAllChildrenAsList() {
        // given
        HashSet<Child> allChildrenAsSet = Sets.newHashSet();
        Child child1 = childFactory.createChild("id1", "name", null);
        Child child2 = childFactory.createChild("id2", "name", null);
        allChildrenAsSet.add(child1);
        allChildrenAsSet.add(child2);

        when(childRepository.getAllChildren()).thenReturn(allChildrenAsSet);

        // when
        List<Child> children = childQuery.allChildren();

        // then
        assertThat(children, is(notNullValue()));
        assertThat(children.size(), is(2));
        assertThat(children.contains(child1), is(true));
        assertThat(children.contains(child2), is(true));
    }

    @Test
    public void shouldReturnChildById() throws EntityNotFoundException {
        // given
        String id = "id";
        Child expectedChild = childFactory.createChild(id, "name", null);
        when(childRepository.findChildById(id)).thenReturn(expectedChild);

        // when
        Child actualChild = childQuery.childById(id);

        // then
        assertThat(actualChild, is(expectedChild));
    }
}
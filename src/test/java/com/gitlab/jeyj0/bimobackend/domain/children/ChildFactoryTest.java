package com.gitlab.jeyj0.bimobackend.domain.children;


import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ChildFactoryTest {

    private ChildFactory childFactory = new ChildFactory();

    @Test
    public void shouldHaveSameValues() {
        // given
        String id = "id";
        String name = "name";
        Set<String> statusIds = Sets.newHashSet();
        Child child = new Child(id, name, statusIds);

        // when
        Child factoryChild = childFactory.createChild(id, name, statusIds);

        // then
        assertThat(factoryChild, is(child));
        assertThat(factoryChild.getId(), is(child.getId()));
        assertThat(factoryChild.getName(), is(child.getName()));
        assertThat(factoryChild.getStatusIds(), is(child.getStatusIds()));
    }

    @Test
    public void shouldGenerateId() {
        // given
        String name = "name";
        Set<String> statusIds = Sets.newHashSet();

        // when
        Child child = childFactory.createChild(null, name, statusIds);

        // then
        assertThat(child.getId(), is(notNullValue()));
        assertThat(child.getName(), is(name));
        assertThat(child.getStatusIds(), is(statusIds));
    }

    @Test
    public void shouldGenerateEmptyStatusIdSet() {
        // given
        String id = "id";
        String name = "name";

        // when
        Child child = childFactory.createChild(id, name, null);

        // then
        assertThat(child.getId(), is(id));
        assertThat(child.getName(), is(name));
        assertThat(child.getStatusIds(), is(notNullValue()));
        assertThat(child.getStatusIds().size(), is(0));
    }

}
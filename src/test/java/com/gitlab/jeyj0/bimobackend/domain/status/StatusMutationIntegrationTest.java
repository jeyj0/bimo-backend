package com.gitlab.jeyj0.bimobackend.domain.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRepository;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = StatusMutationIntegrationTestConfig.class)
public class StatusMutationIntegrationTest {

    @Autowired
    private StatusMutation statusMutation;

    @Autowired
    private StatusQuery statusQuery;

    @Autowired
    private EventRepository eventRepository;

    @Test
    public void shouldCreateStatus() throws ClassNotFoundException, DuplicateIdException, JsonProcessingException {
        // given
        String name = "name";

        // when
        Status status = statusMutation.createStatus(name);

        // then
        assertThat(status.getName(), is(name));
        assertThat(status.getId(), is(notNullValue()));
    }

    @Test
    public void shouldRenameStatus() throws ClassNotFoundException, DuplicateIdException, JsonProcessingException, EntityNotFoundException {
        // given
        String currentName = "currentName";
        Status currentStatus = statusMutation.createStatus(currentName);
        String updatedName = "updatedName";

        // when
        Status updatedStatus = statusMutation.renameStatus(currentStatus.getId(), updatedName);

        // then
        assertThat(updatedStatus.getName(), is(updatedName));
        assertThat(updatedStatus.getId(), is(currentStatus.getId()));
    }

    @Test
    public void shouldDeleteStatus() throws ClassNotFoundException, DuplicateIdException, JsonProcessingException, EntityNotFoundException {
        // given
        Status status = statusMutation.createStatus("name");

        // when
        statusMutation.deleteStatus(status.getId());

        // then
        assertThat(statusQuery.allStatuses().contains(status), is(false));
    }
}

package com.gitlab.jeyj0.bimobackend.domain.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StringToStatusObjectConverterTest {

    private static final String STATUS_AS_STRING = "{\"id\":\"id\",\"name\":\"name\"}";
    private static final String STATUS_ID = "id";
    private static final String STATUS_NAME = "name";

    @InjectMocks
    private StringToStatusObjectConverter stringToStatusObjectConverter;

    @Mock
    private StatusFactory statusFactory;

    @Test
    public void shouldConvertStringToStatus() throws IOException {
        // given
        when(statusFactory.createStatus(STATUS_ID, STATUS_NAME)).thenReturn(givenStatus());

        // when
        Status status = stringToStatusObjectConverter.convertStringToObject(STATUS_AS_STRING);

        // then
        assertThat(status, is(givenStatus()));
    }

    private Status givenStatus() {
        return new Status(STATUS_ID, STATUS_NAME);
    }
}

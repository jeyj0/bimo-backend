package com.gitlab.jeyj0.bimobackend.domain.children;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChildRepositoryTest {

    private static final Child CHILD_IN_REPOSITORY = new Child("childId1", "childName1", Sets.newHashSet());
    private static final Set<Child> GIVEN_CHILDREN = Set.of(
            CHILD_IN_REPOSITORY,
            new Child("id2", "childName2", Sets.newHashSet()),
            new Child("id3", "childName2", Sets.newHashSet())
    );

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private ChildRepository childRepository;

    @Mock
    private ChildFactory childFactory;

    @Mock
    private GenericEventSourcingBasedRepository<Child> genericEventSourcingBasedRepository;

    @Mock
    private ChildObjectToStringConverter childObjectToStringConverter;

    @Test
    public void shouldFindChildById() throws EntityNotFoundException {
        // given
        when(genericEventSourcingBasedRepository.getAllEntities()).thenReturn(GIVEN_CHILDREN);

        // when
        Child foundChild = childRepository.findChildById(CHILD_IN_REPOSITORY.getId());

        // then
        assertThat(foundChild, is(CHILD_IN_REPOSITORY));
    }

    @Test
    public void shouldThrowExceptionIfChildIsNotFound() throws EntityNotFoundException {
        // given
        when(genericEventSourcingBasedRepository.getAllEntities()).thenReturn(GIVEN_CHILDREN);

        // expect
        expectedException.expect(EntityNotFoundException.class);

        // when
        childRepository.findChildById("nonExistentId");
    }

    @Test
    public void shouldCreateChild() throws JsonProcessingException, DuplicateIdException, ClassNotFoundException {
        // given
        String childId = "childId";
        String childName = "childName";
        Child expectedChild = new Child(childId, childName, Sets.newHashSet());

        when(childFactory.createChildWithoutIdAndStatuses(childName)).thenReturn(expectedChild);

        // when
        Child actualChild = childRepository.createChild(childName);

        // then
        assertThat(actualChild, is(expectedChild));
    }

    @Test
    public void shouldUpdateChild() throws EntityNotFoundException, DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        // given
        when(genericEventSourcingBasedRepository.getAllEntities()).thenReturn(GIVEN_CHILDREN);

        String childId = CHILD_IN_REPOSITORY.getId();
        String newName = "newChildName";
        Child expectedUpdatedChild = new Child(childId, newName, CHILD_IN_REPOSITORY.getStatusIds());

        when(childFactory.createChild(childId, newName, CHILD_IN_REPOSITORY.getStatusIds())).thenReturn(expectedUpdatedChild);

        // when
        Child updatedChild = childRepository.updateChild(childId, newName);

        // then
        assertThat(updatedChild, is(expectedUpdatedChild));
        verify(genericEventSourcingBasedRepository).updateEntity(CHILD_IN_REPOSITORY, expectedUpdatedChild, childObjectToStringConverter);
    }

    @Test
    public void deleteChildShouldThrowEntityNotFoundException() throws EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        // expect
        expectedException.expect(EntityNotFoundException.class);

        // when
        childRepository.deleteChild("id");
    }

    @Test
    public void shouldDeleteChild() throws JsonProcessingException, EntityNotFoundException, ClassNotFoundException {
        // given
        when(genericEventSourcingBasedRepository.getAllEntities()).thenReturn(GIVEN_CHILDREN);

        // when
        boolean hasDeletionBeenSuccessful = childRepository.deleteChild(CHILD_IN_REPOSITORY.getId());

        // then
        verify(genericEventSourcingBasedRepository).deleteEntity(CHILD_IN_REPOSITORY, childObjectToStringConverter);
        assertThat(hasDeletionBeenSuccessful, is(true));
    }
}
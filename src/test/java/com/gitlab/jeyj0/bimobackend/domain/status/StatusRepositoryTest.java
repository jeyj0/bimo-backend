package com.gitlab.jeyj0.bimobackend.domain.status;

import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatusRepositoryTest {

    private static final StatusFactory STATUS_FACTORY = new StatusFactory();
    private static final Status STATUS_1 = STATUS_FACTORY.createStatus("id1", "name1");
    private static final Status STATUS_2 = STATUS_FACTORY.createStatus("id2", "name2");
    private static final Set<Status> ALL_STATUS = Sets.newHashSet(
            STATUS_1,
            STATUS_2,
            STATUS_FACTORY.createStatus("id3", "name3")
    );

    @InjectMocks
    private StatusRepository statusRepository;

    @Mock
    private GenericEventSourcingBasedRepository<Status> genericEventSourcingBasedRepository;

    @Test
    public void shouldGetAllEntities() {
        // given
        when(genericEventSourcingBasedRepository.getAllEntities()).thenReturn(ALL_STATUS);

        // when
        Set<Status> allStatus = statusRepository.getAllStatus();

        // then
        assertThat(allStatus, is(ALL_STATUS));
    }

    @Test
    public void shouldFindStatusesByIds() {
        // given
        when(genericEventSourcingBasedRepository.getAllEntities()).thenReturn(ALL_STATUS);
        Set<String> statusIds = Set.of(STATUS_1.getId(), STATUS_2.getId());

        // when
        Set<Status> statuses = statusRepository.findStatusesByIds(statusIds);

        // then
        assertThat(statuses.size(), is(statusIds.size()));
        assertThat(statuses.contains(STATUS_1), is(true));
        assertThat(statuses.contains(STATUS_2), is(true));
    }

    @Test
    public void findStatusByIdsShouldNotModifyInputIds() {
        // given
        Set<String> inputStatusIds = Sets.newHashSet();
        inputStatusIds.add(STATUS_1.getId());
        inputStatusIds.add(STATUS_2.getId());
        Set<String> inputStatusIdsCopy = Set.of(STATUS_1.getId(), STATUS_2.getId());

        // when
        statusRepository.findStatusesByIds(inputStatusIds);

        // then
        assertThat(inputStatusIds.size(), is(inputStatusIdsCopy.size()));
        assertThat(inputStatusIds.contains(STATUS_1.getId()), is(true));
        assertThat(inputStatusIds.contains(STATUS_2.getId()), is(true));
    }
}

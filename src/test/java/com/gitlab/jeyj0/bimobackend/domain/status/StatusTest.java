package com.gitlab.jeyj0.bimobackend.domain.status;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class StatusTest {

    private static final String ID = "id";
    private static final String NAME = "name";

    @Test
    public void shouldBeEqual() {
        // given
        Status status = givenStatus();
        Status otherStatus = givenStatus();

        // when
        boolean isEqual = status.equals(otherStatus);

        // then
        assertThat(isEqual, is(true));
    }

    @Test
    public void shouldNotBeEqualIfNull() {
        // given
        Status status = givenStatus();

        // when
        @SuppressWarnings("ObjectEqualsNull") boolean isEqual = status.equals(null);

        // then
        //noinspection ConstantConditions
        assertThat(isEqual, is(false));
    }

    @Test
    public void shouldNotBeEqualIfIdDiffers() {
        // given
        Status status = givenStatus();
        Status otherStatus = new Status("other" + ID, NAME);

        // when
        boolean isEqual = status.equals(otherStatus);

        // then
        assertThat(isEqual, is(false));
    }

    @Test
    public void shouldNotBeEqualIfNameDiffers() {
        // given
        Status status = givenStatus();
        Status otherStatus = new Status(ID, "other" + NAME);

        // when
        boolean isEqual = status.equals(otherStatus);

        // then
        assertThat(isEqual, is(false));
    }

    @Test
    public void getName() {
        // given
        Status status = givenStatus();

        // when
        String name = status.getName();

        // then
        assertThat(name, is(NAME));
    }

    private Status givenStatus() {
        return new Status(ID, NAME);
    }
}
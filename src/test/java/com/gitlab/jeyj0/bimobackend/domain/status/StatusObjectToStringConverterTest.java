package com.gitlab.jeyj0.bimobackend.domain.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class StatusObjectToStringConverterTest {

    private static final String STATUS_AS_STRING = "{\"id\":\"id\",\"name\":\"name\"}";
    private static final String STATUS_ID = "id";
    private static final String STATUS_NAME = "name";

    private StatusObjectToStringConverter statusObjectToStringConverter = new StatusObjectToStringConverter();

    @Test
    public void shouldConvertStatusToString() throws JsonProcessingException {
        // when
        String statusAsString = statusObjectToStringConverter.convertObjectToString(givenStatus());

        // then
        assertThat(statusAsString, is(STATUS_AS_STRING));
    }

    private Status givenStatus() {
        return new Status(STATUS_ID, STATUS_NAME);
    }
}

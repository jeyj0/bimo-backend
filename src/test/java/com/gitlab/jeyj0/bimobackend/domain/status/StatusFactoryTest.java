package com.gitlab.jeyj0.bimobackend.domain.status;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

public class StatusFactoryTest {

    private StatusFactory statusFactory = new StatusFactory();

    @Test
    public void shouldHaveSameValues() {
        // given
        String id = "id";
        String name = "name";
        Status expectedStatus = new Status(id, name);

        // when
        Status actualStatus = statusFactory.createStatus(id, name);

        // then
        assertThat(actualStatus, is(expectedStatus));
        assertThat(actualStatus.getId(), is(expectedStatus.getId()));
        assertThat(actualStatus.getName(), is(expectedStatus.getName()));
    }

    @Test
    public void shouldGenerateId() {
        // given
        String name = "name";

        // when
        Status status = statusFactory.createStatus(null, name);

        // then
        assertThat(status.getId(), is(notNullValue()));
        assertThat(status.getName(), is(name));
    }
}
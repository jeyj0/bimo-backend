package com.gitlab.jeyj0.bimobackend.domain.children;

import com.gitlab.jeyj0.bimobackend.domain.status.Status;
import com.gitlab.jeyj0.bimobackend.domain.status.StatusFactory;
import com.gitlab.jeyj0.bimobackend.domain.status.StatusRepository;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChildResolverTest {

    @InjectMocks
    private ChildResolver childResolver;

    @Mock
    private StatusRepository statusRepository;

    @Test
    public void shouldReturnStatusSetAsList() {
        // given
        Set<Status> statusSet = Sets.newHashSet();
        Set<String> statusIdSet = Sets.newHashSet();
        List<Status> expectedStatusList = Lists.newArrayList();

        Status status = new StatusFactory().createStatus(null, "name");
        statusSet.add(status);
        statusIdSet.add(status.getId());
        expectedStatusList.add(status);

        Child child = new ChildFactory().createChild(null, null, statusIdSet);

        when(statusRepository.findStatusesByIds(statusIdSet)).thenReturn(statusSet);

        // when
        List<Status> actualStatusList = childResolver.statuses(child);

        // then
        assertThat(actualStatusList, is(expectedStatusList));
    }
}
package com.gitlab.jeyj0.bimobackend.domain.children;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ChildMutationIntegrationTestConfig.class)
public class ChildMutationIntegrationTest {

    @Autowired
    private ChildMutation childMutation;

    @Test
    public void shouldAddStatusToChild() throws ClassNotFoundException, DuplicateIdException, EntityNotFoundException, JsonProcessingException {
        // given
        Child currentChild = childMutation.createChild("childName");
        String statusId = "statusId";

        // when
        Child updatedChild = childMutation.addStatusToChild(currentChild.getId(), statusId);

        // then
        assertThat(updatedChild, is(notNullValue()));
        assertThat(updatedChild.getStatusIds().contains(statusId), is(true));
    }

    @Test
    public void shouldRemoveStatusFromChild() throws ClassNotFoundException, DuplicateIdException, JsonProcessingException, EntityNotFoundException {
        // given
        Child currentChild = childMutation.createChild("childName");
        String statusId = "statusId";
        currentChild = childMutation.addStatusToChild(currentChild.getId(), statusId);

        // when
        Child updatedChild = childMutation.removeStatusFromChild(currentChild.getId(), statusId);

        // then
        assertThat(updatedChild, is(notNullValue()));
        assertThat(updatedChild.getStatusIds().contains(statusId), is(false));
    }
}

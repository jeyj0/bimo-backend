package com.gitlab.jeyj0.bimobackend.domain.status;

import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatusQueryTest {

    @InjectMocks
    private StatusQuery statusQuery;

    @Mock
    private StatusRepository statusRepository;

    private StatusFactory statusFactory = new StatusFactory();

    @Test
    public void shouldReturnAllStatusesAsList() {
        // given
        Set<Status> expectedStatuses = Sets.newHashSet();
        Status status1 = statusFactory.createStatus("id1", "name1");
        Status status2 = statusFactory.createStatus("id2", "name2");
        expectedStatuses.add(status1);
        expectedStatuses.add(status2);

        when(statusRepository.getAllStatus()).thenReturn(expectedStatuses);

        // when
        List<Status> actualStatuses = statusQuery.allStatuses();

        // then
        assertThat(actualStatuses, is(notNullValue()));
        assertThat(actualStatuses.size(), is(2));
        assertThat(actualStatuses.contains(status1), is(true));
        assertThat(actualStatuses.contains(status2), is(true));
    }
}
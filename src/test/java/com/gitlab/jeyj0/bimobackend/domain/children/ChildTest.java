package com.gitlab.jeyj0.bimobackend.domain.children;

import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ChildTest {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final Set<String> STATUS_IDS = Sets.newHashSet();

    @Test
    public void shouldBeEqual() {
        // given
        Child child1 = givenChild();
        Child child2 = givenChild();

        // when
        boolean firstResult = child1.equals(child2);
        boolean secondResult = child2.equals(child1);

        // then
        assertThat(firstResult, is(true));
        assertThat(secondResult, is(true));
    }

    @Test

    public void shouldNotBeEqualIfNull() {
        // given
        Child child = givenChild();

        // when
        @SuppressWarnings("ObjectEqualsNull") boolean isEqual = child.equals(null);

        // then
        //noinspection ConstantConditions
        assertThat(isEqual, is(false));
    }

    @Test
    public void shouldNotBeEqualIfWrongClass() {
        // given
        Child child = givenChild();
        Object other = new Object();

        // when
        boolean isEqual = child.equals(other);

        // then
        assertThat(isEqual, is(false));
    }

    @Test
    public void shouldNotBeEqualIfIdDiffers() {
        // given
        Child child = givenChild();
        Child otherChild = new Child("wrongId", NAME, STATUS_IDS);

        // when
        boolean isEqual = child.equals(otherChild);

        // then
        assertThat(isEqual, is(false));
    }

    @Test
    public void shouldNotBeEqualIfNameDiffers() {
        // given
        Child child = givenChild();
        Child otherChild = new Child(ID, "wrongName", STATUS_IDS);

        // when
        boolean isEqual = child.equals(otherChild);

        // then
        assertThat(isEqual, is(false));
    }

    @Test
    public void shouldNotBeEqualIfStatusIdsDiffer() {
        // given
        Child child = givenChild();
        Set<String> otherStatusIds = Sets.newHashSet();
        otherStatusIds.add(null);
        Child otherChild = new Child(ID, NAME, otherStatusIds);

        // when
        boolean isEqual = child.equals(otherChild);

        // then
        assertThat(isEqual, is(false));
    }

    @Test
    public void shouldGetCorrectName() {
        // given
        Child child = givenChild();

        // when
        String name = child.getName();

        // then
        assertThat(name, is(NAME));
    }

    @Test
    public void shouldGetCorrectStatusIds() {
        // given
        Child child = givenChild();

        // when
        Set<String> statusIds = child.getStatusIds();

        // then
        assertThat(statusIds, is(STATUS_IDS));
    }

    private Child givenChild() {
        return new Child(ID, NAME, STATUS_IDS);
    }
}
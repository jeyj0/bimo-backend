package com.gitlab.jeyj0.bimobackend.domain.status;

import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRegistry;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRepository;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.created.CreatedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted.DeletedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated.UpdatedEventFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
public class StatusMutationIntegrationTestConfig {

    @Bean
    public StatusMutation statusMutation() {
        return new StatusMutation();
    }

    @Bean
    public StatusRepository statusRepository() {
        return new StatusRepository();
    }

    @Bean
    public GenericEventSourcingBasedRepository<Status> genericEventSourcingBasedRepository() {
        return new GenericEventSourcingBasedRepository<>();
    }

    @Bean
    public EventRegistry eventRegistry() {
        return new EventRegistry();
    }

    @Bean
    public EventRepository eventRepository() {
        return mock(EventRepository.class);
    }

    @Bean
    public EventFactory eventFactory() {
        return new EventFactory();
    }

    @Bean
    public CreatedEventFactory createdEventFactory() {
        return new CreatedEventFactory();
    }

    @Bean
    public UpdatedEventFactory updatedEventFactory() {
        return new UpdatedEventFactory();
    }

    @Bean
    public DeletedEventFactory deletedEventFactory() {
        return new DeletedEventFactory();
    }

    @Bean
    public StatusFactory statusFactory() {
        return new StatusFactory();
    }

    @Bean
    public StatusObjectToStringConverter statusObjectToStringConverter() {
        return new StatusObjectToStringConverter();
    }

    @Bean
    public StatusQuery statusQuery() {
        return new StatusQuery();
    }
}

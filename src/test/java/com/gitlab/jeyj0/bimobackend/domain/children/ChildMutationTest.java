package com.gitlab.jeyj0.bimobackend.domain.children;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChildMutationTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private ChildMutation childMutation;

    @Mock
    private ChildRepository childRepository;

    private ChildFactory childFactory = new ChildFactory();

    @Test
    public void createChildShouldThrowNullPointerExceptionIfNameIsNull() throws IOException, ClassNotFoundException {
        // expect
        expectedException.expect(NullPointerException.class);

        // when
        childMutation.createChild(null);
    }

    @Test
    public void createChildShouldCreateChild() throws IOException, ClassNotFoundException {
        // given
        String childName = "name";
        Child expectedChild = childFactory.createChildWithoutIdAndStatuses(childName);
        when(childRepository.createChild(childName)).thenReturn(expectedChild);

        // when
        Child actualChild = childMutation.createChild(childName);

        // then
        verify(childRepository).createChild(childName);
        assertThat(actualChild, is(notNullValue()));
        assertThat(actualChild.getName(), is(childName));
        assertThat(actualChild, is(expectedChild));
    }

    @Test
    public void renameChildShouldUpdateChild() throws EntityNotFoundException, DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        // given
        String childId = "id";
        String newChildName = "newName";
        Child expectedChild = new Child(childId, newChildName, Sets.newHashSet());
        when(childRepository.updateChild(childId, newChildName)).thenReturn(expectedChild);

        // when
        Child actualChild = childMutation.renameChild(childId, newChildName);

        // then
        verify(childRepository).updateChild(childId, newChildName);
        assertThat(actualChild, is(notNullValue()));
        assertThat(actualChild.getName(), is(newChildName));
        assertThat(actualChild, is(expectedChild));
    }

    @Test
    public void renameChildShouldThrowNullPointerWhenIdIsNull() throws EntityNotFoundException, DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        // expect
        expectedException.expect(NullPointerException.class);

        // when
        childMutation.renameChild(null, "name");
    }

    @Test
    public void renameChildShouldThrowNullPointerWhenNameIsNull() throws EntityNotFoundException, DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        // expect
        expectedException.expect(NullPointerException.class);

        // when
        childMutation.renameChild("id", null);
    }

    @Test
    public void deleteChildShouldThrowNullPointerWhenIdIsNull() throws EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        // expect
        expectedException.expect(NullPointerException.class);

        // when
        childMutation.deleteChild(null);
    }

    @Test
    public void deleteChildShouldDeleteChild() throws EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        // given
        String childId = "id";
        when(childRepository.deleteChild(childId)).thenReturn(true);

        // when
        boolean hasDeletionBeenSuccessful = childMutation.deleteChild(childId);

        // then
        verify(childRepository).deleteChild(childId);
        assertThat(hasDeletionBeenSuccessful, is(true));
    }

    @Test
    public void addStatusToChildShouldThrowExceptionIfChildIdIsNull() throws ClassNotFoundException, DuplicateIdException, EntityNotFoundException, JsonProcessingException {
        // given
        String statusId = "statusId";

        // expect
        expectedException.expect(NullPointerException.class);

        // when
        childMutation.addStatusToChild(null, statusId);
    }

    @Test
    public void addStatusToChildShouldThrowExceptionIfStatusIdIsNull() throws ClassNotFoundException, DuplicateIdException, EntityNotFoundException, JsonProcessingException {
        // given
        String childId = "childId";

        // expect
        expectedException.expect(NullPointerException.class);

        // when
        childMutation.addStatusToChild(childId, null);
    }

    @Test
    public void removeStatusFromChildShouldThrowExceptionIfChildIdIsNull() throws ClassNotFoundException, DuplicateIdException, EntityNotFoundException, JsonProcessingException {
        // given
        String statusId = "statusId";

        // expect
        expectedException.expect(NullPointerException.class);

        // when
        childMutation.removeStatusFromChild(null, statusId);
    }

    @Test
    public void removeStatusFromChildShouldThrowExceptionIfStatusIdIsNull() throws ClassNotFoundException, DuplicateIdException, EntityNotFoundException, JsonProcessingException {
        // given
        String childId = "childId";

        // expect
        expectedException.expect(NullPointerException.class);

        // when
        childMutation.removeStatusFromChild(childId, null);
    }
}
package com.gitlab.jeyj0.bimobackend.eventsourcing.events;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventRegistryTest {

    @InjectMocks
    private EventRegistry eventRegistry;

    @Mock
    private EventRepository eventRepository;

    @Mock
    private EventFactory eventFactory;

    @Mock
    private ApplicationContext applicationContext;

    @Test
    public void shouldSaveFiredEvent() throws ClassNotFoundException {
        // given
        Event<DBEntity> event = new Event<>();
        when(eventFactory.createEventWithCurrentTimestamp(event)).thenReturn(event);

        // when
        eventRegistry.fireEvent(event);

        // then
        verify(eventRepository).save(event);
    }

    @Test
    public void shouldReplayEvent() throws IOException, ClassNotFoundException {
        // given
        Event<EntityForTest> event = new Event<>(EventType.CREATED, EntityForTest.class, "someData");

        GenericEventSourcingBasedRepository<EntityForTest> genericEventSourcingBasedRepository = mock(GenericEventSourcingBasedRepository.class);
        when(applicationContext.getBean("genericEventSourcingBasedRepositoryForEntityForTest")).thenReturn(genericEventSourcingBasedRepository);

        // when
        eventRegistry.replayEvent(event);

        // then
        verify(genericEventSourcingBasedRepository).replayEvent(event);
    }

    public class EntityForTest extends DBEntity {

        public EntityForTest(String id) {
            super(id);
        }

        @Override
        public boolean equals(Object obj) {
            return false;
        }
    }
}
package com.gitlab.jeyj0.bimobackend.eventsourcing.events.created;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import org.junit.Test;

import static com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventType.CREATED;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CreatedEventFactoryTest {

    private CreatedEventFactory createdEventFactory = new CreatedEventFactory();

    @Test
    public void shouldCreateCreatedEvent() {
        // given
        String entityAsString = "{\"id\":\"entityId\"}";
        CreatedEvent<EntityForTest> expectedCreatedEvent = new CreatedEvent<>(EntityForTest.class, entityAsString);

        // when
        CreatedEvent<EntityForTest> actualCreatedEvent = createdEventFactory.createCreatedEvent(EntityForTest.class, entityAsString);

        // then
        assertThat(actualCreatedEvent.getClazz(), is(expectedCreatedEvent.getClazz()));
        assertThat(actualCreatedEvent.getEventType(), is(CREATED));
        assertThat(actualCreatedEvent.getData(), is(expectedCreatedEvent.getData()));
    }

    @Test
    public void shouldCreateCreatedEventByEntityAndObjectStringConverter() throws JsonProcessingException {
        // given
        String entityId = "id";
        EntityForTest entityForTest = new EntityForTest(entityId);
        ObjectToStringConverterForTest objectToStringConverterForTest = new ObjectToStringConverterForTest();
        CreatedEvent<EntityForTest> expectedCreatedEvent = new CreatedEvent<>(EntityForTest.class, "{\"id\":\""+ entityId +"\"}");

        // when
        CreatedEvent<EntityForTest> actualCreatedEvent = createdEventFactory.createCreatedEventByEntityAndObjectStringConverter(entityForTest, objectToStringConverterForTest);

        // then
        assertThat(actualCreatedEvent.getClazz(), is(expectedCreatedEvent.getClazz()));
        assertThat(actualCreatedEvent.getEventType(), is(CREATED));
        assertThat(actualCreatedEvent.getData(), is(expectedCreatedEvent.getData()));
    }

    private class EntityForTest extends DBEntity {

        public EntityForTest(String id) {
            super(id);
        }

        @Override
        public boolean equals(Object obj) {
            return obj != null &&
                    obj instanceof EntityForTest &&
                    ((EntityForTest) obj).getId().equals(this.getId());
        }
    }

    private class ObjectToStringConverterForTest implements ObjectToStringConverter<EntityForTest> {

        @Override
        public String convertObjectToString(EntityForTest entity) throws JsonProcessingException {
            return "{\"id\":\"" + entity.getId() + "\"}";
        }
    }
}
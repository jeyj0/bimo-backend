package com.gitlab.jeyj0.bimobackend.eventsourcing;


import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static com.gitlab.jeyj0.bimobackend.eventsourcing.IDGenerator.generateNewId;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class IDGeneratorTest {

    @Test
    public void shouldReturn10000UniqueIds() {
        List<String> ids = Lists.newArrayListWithCapacity(100);
        for (int i = 0; i < 10000; i++)
            ids.add(generateNewId());

        long duplicates = ids.stream().parallel().filter(id -> occurrences(ids, id) != 1).count();
        assertThat(duplicates, is(0L));
    }

    private long occurrences(List<String> list, String element) {
        return list.stream().parallel().filter(e -> e.equals(element)).count();
    }
}

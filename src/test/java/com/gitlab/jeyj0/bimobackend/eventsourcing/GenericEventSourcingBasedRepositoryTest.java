package com.gitlab.jeyj0.bimobackend.eventsourcing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.*;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.created.CreatedEvent;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.created.CreatedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted.DeletedEvent;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted.DeletedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated.UpdatedEvent;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated.UpdatedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GenericEventSourcingBasedRepositoryTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private GenericEventSourcingBasedRepositoryForTest tableForTest = new GenericEventSourcingBasedRepositoryForTest();

    @Mock
    private EventRegistry eventRegistry;

    @Mock
    private CreatedEventFactory createdEventFactory;

    @Mock
    private UpdatedEventFactory updatedEventFactory;

    @Mock
    private DeletedEventFactory deletedEventFactory;

    private ObjectToStringConverterForTest objectToStringConverterForTest = new ObjectToStringConverterForTest();

    @Test
    public void shouldCreateEntity() throws DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        // given
        EntityForTest entity = new EntityForTest("id");
        String entityAsString = "{\"id\":\"" + "id" + "\",\"name\":\"" + entity.getName() + "\"}";
        CreatedEvent<EntityForTest> expectedCreatedEvent = new CreatedEventForTest<>(EntityForTest.class, entityAsString);
        when(createdEventFactory.createCreatedEventByEntityAndObjectStringConverter(entity, objectToStringConverterForTest)).thenReturn(expectedCreatedEvent);

        // when
        tableForTest.createEntity(entity, objectToStringConverterForTest);

        // then
        assertThat(tableForTest.getAllEntities().contains(entity), is(true));
        verify(eventRegistry).fireEvent(expectedCreatedEvent);
    }

    @Test
    public void shouldThrowDuplicateIdException() throws DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        // given
        EntityForTest entity1 = new EntityForTest("id");
        EntityForTest entity2 = new EntityForTest("id");
        tableForTest.createEntity(entity1, objectToStringConverterForTest);

        // expect
        expectedException.expect(DuplicateIdException.class);

        // when
        tableForTest.createEntity(entity2, objectToStringConverterForTest);
    }

    @Test
    public void shouldUpdateEntity() throws DuplicateIdException, EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        // given
        EntityForTest currentEntity = new EntityForTest("id", "currentEntity");
        EntityForTest updatedEntity = new EntityForTest("id", "updatedEntity");
        String currentEntityAsString = "{\"id\":\"" + currentEntity.getId() + "\",\"name\":\"" + currentEntity.getName() + "\"}";
        String updatedEntityAsString = "{\"id\":\"" + updatedEntity.getId() + "\",\"name\":\"" + updatedEntity.getName() + "\"}";
        String eventData = "{\"current\":" + currentEntityAsString + ",\"updated\":" + updatedEntityAsString + "}";
        UpdatedEvent<EntityForTest> expectedUpdatedEvent = new UpdatedEventForTest<>(EntityForTest.class, eventData);
        tableForTest.createEntity(currentEntity, objectToStringConverterForTest);

        when(updatedEventFactory.createUpdatedEventByEntitiesAndObjectStringConverter(currentEntity, updatedEntity, objectToStringConverterForTest)).thenReturn(expectedUpdatedEvent);

        // when
        tableForTest.updateEntity(currentEntity, updatedEntity, objectToStringConverterForTest);

        // then
        assertThat(tableForTest.getAllEntities().contains(currentEntity), is(false));
        assertThat(tableForTest.getAllEntities().contains(updatedEntity), is(true));
        verify(eventRegistry).fireEvent(expectedUpdatedEvent);
    }

    @Test
    public void shouldThrowExceptionIfEntityToUpdateDoesNotExist() throws EntityNotFoundException, DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        // given
        EntityForTest currentEntity = new EntityForTest("id", "currentEntity");
        EntityForTest updatedEntity = new EntityForTest("id", "updatedEntity");

        // expect
        expectedException.expect(EntityNotFoundException.class);

        // when
        tableForTest.updateEntity(currentEntity, updatedEntity, objectToStringConverterForTest);
    }

    @Test
    public void shouldThrowExceptionIfEntityToDeleteDoesNotExist() throws EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        // given
        EntityForTest entity = new EntityForTest("id", "entity");

        // expect
        expectedException.expect(EntityNotFoundException.class);

        // when
        tableForTest.deleteEntity(entity, objectToStringConverterForTest);
    }

    @Test
    public void shouldDeleteEntity() throws JsonProcessingException, DuplicateIdException, EntityNotFoundException, ClassNotFoundException {
        // given
        EntityForTest entity = new EntityForTest("id", "entity");
        tableForTest.createEntity(entity, objectToStringConverterForTest);

        // when
        tableForTest.deleteEntity(entity, objectToStringConverterForTest);

        // then
        assertThat(tableForTest.getAllEntities().contains(entity), is(false));
    }

    @Test
    public void shouldFireDeletedEvent() throws JsonProcessingException, DuplicateIdException, EntityNotFoundException, ClassNotFoundException {
        // given
        EntityForTest entity = new EntityForTest("id", "entity");
        tableForTest.createEntity(entity, objectToStringConverterForTest);

        String entityAsString = objectToStringConverterForTest.convertObjectToString(entity);
        DeletedEvent<EntityForTest> expectedDeletedEvent = new DeletedEventForTest<>(EntityForTest.class, entityAsString);

        when(deletedEventFactory.createDeletedEventByEntityAndObjectStringConverter(entity, objectToStringConverterForTest)).thenReturn(expectedDeletedEvent);

        // when
        tableForTest.deleteEntity(entity, objectToStringConverterForTest);

        // then
        verify(eventRegistry).fireEvent(expectedDeletedEvent);
    }

    private class GenericEventSourcingBasedRepositoryForTest extends GenericEventSourcingBasedRepository<EntityForTest> {
    }

    private class EntityForTest extends DBEntity {
        private final String name;

        EntityForTest(String id, String name) {
            super(id);
            this.name = name;
        }

        EntityForTest(String id) {
            this(id, "name");
        }

        public String getName() {
            return name;
        }

        @Override
        public boolean equals(Object obj) {
            return obj != null &&
                    obj instanceof EntityForTest &&
                    ((EntityForTest) obj).getId().equals(this.getId()) &&
                    ((EntityForTest) obj).getName().equals(this.getName());
        }
    }

    private class ObjectToStringConverterForTest implements ObjectToStringConverter<EntityForTest> {

        @Override
        public String convertObjectToString(EntityForTest entity) {
            return "{\"id\":\"" + entity.getId() + "\",\"name\":\"" + entity.getName() + "\"}";
        }
    }

    private class CreatedEventForTest<ENTITY extends DBEntity> extends CreatedEvent<ENTITY> {
        CreatedEventForTest(Class<ENTITY> entityType, String entityAsString) {
            super(entityType, entityAsString);
        }
    }

    private class UpdatedEventForTest<ENTITY extends DBEntity> extends UpdatedEvent<ENTITY> {
        UpdatedEventForTest(Class<ENTITY> entityType, String data) {
            super(entityType, data);
        }
    }

    private class DeletedEventForTest<ENTITY extends DBEntity> extends DeletedEvent<ENTITY> {
        DeletedEventForTest(Class<ENTITY> entityType, String entityAsString) {
            super(entityType, entityAsString);
        }
    }
}
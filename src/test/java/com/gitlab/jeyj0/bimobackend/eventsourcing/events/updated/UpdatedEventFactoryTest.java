package com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import org.junit.Test;

import static com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventType.UPDATED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class UpdatedEventFactoryTest {

    private UpdatedEventFactory updatedEventFactory = new UpdatedEventFactory();

    @Test
    public void shouldCreateUpdatedEvent() {
        // given
        String entityId = "id";
        String oldName = "oldName";
        String newName = "newName";

        EntityForTest currentEntity = new EntityForTest(entityId, oldName);
        EntityForTest updatedEntity = new EntityForTest(entityId, newName);

        String currentEntityAsString = "{\"id\":\"" + entityId + "\",\"name\":\"" + oldName + "\"}";
        String updatedEntityAsString = "{\"id\":\"" + entityId + "\",\"name\":\"" + newName + "\"}";
        String expectedUpdatedEventData = "{\"current\":" + currentEntityAsString + ",\"updated\":" + updatedEntityAsString + "}";

        UpdatedEvent<EntityForTest> expectedUpdatedEvent = new UpdatedEvent<>(EntityForTest.class, expectedUpdatedEventData);

        // when
        UpdatedEvent<EntityForTest> actualUpdatedEvent = updatedEventFactory.createUpdatedEvent(EntityForTest.class, currentEntityAsString, updatedEntityAsString);

        assertThat(actualUpdatedEvent.getClazz(), is(expectedUpdatedEvent.getClazz()));
        assertThat(actualUpdatedEvent.getEventType(), is(UPDATED));
        assertThat(actualUpdatedEvent.getData(), is(expectedUpdatedEvent.getData()));
    }

    @Test
    public void shouldCreateUpdatedEventByEntityAndObjectStringConverter() throws JsonProcessingException {
        // given
        String entityId = "id";
        String oldName = "oldName";
        String newName = "newName";

        EntityForTest currentEntity = new EntityForTest(entityId, oldName);
        EntityForTest updatedEntity = new EntityForTest(entityId, newName);

        String currentEntityAsString = "{\"id\":\"" + entityId + "\",\"name\":\"" + oldName + "\"}";
        String updatedEntityAsString = "{\"id\":\"" + entityId + "\",\"name\":\"" + newName + "\"}";
        String expectedUpdatedEventData = "{\"current\":" + currentEntityAsString + ",\"updated\":" + updatedEntityAsString + "}";

        UpdatedEvent<EntityForTest> expectedUpdatedEvent = new UpdatedEvent<>(EntityForTest.class, expectedUpdatedEventData);

        ObjectToStringConverterForTest objectToStringConverter = new ObjectToStringConverterForTest();

        // when
        UpdatedEvent<EntityForTest> actualUpdatedEvent = updatedEventFactory.createUpdatedEventByEntitiesAndObjectStringConverter(currentEntity, updatedEntity, objectToStringConverter);

        // then
        assertThat(actualUpdatedEvent.getClazz(), is(expectedUpdatedEvent.getClazz()));
        assertThat(actualUpdatedEvent.getEventType(), is(UPDATED));
        assertThat(actualUpdatedEvent.getData(), is(expectedUpdatedEvent.getData()));
    }

    private class EntityForTest extends DBEntity {

        private final String name;

        public EntityForTest(String id, String name) {
            super(id);
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public boolean equals(Object obj) {
            return obj != null &&
                    obj instanceof EntityForTest &&
                    ((EntityForTest) obj).getId().equals(getId());
        }
    }

    private class ObjectToStringConverterForTest implements ObjectToStringConverter<EntityForTest> {

        @Override
        public String convertObjectToString(EntityForTest entity) {
            return "{\"id\":\"" + entity.getId() + "\",\"name\":\"" + entity.getName() + "\"}";
        }
    }
}
package com.gitlab.jeyj0.bimobackend.eventsourcing.events;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class TimeStampTest {

    @Test
    public void shouldGetCurrentTime() {
        // given
        List<TimeStamp> timeStamps = Lists.newArrayList();

        // when
        for (int i = 0; i < 100; i++)
            timeStamps.add(TimeStamp.getCurrent());

        // then
        for (int i = 0; i < timeStamps.size(); i++) {
            for (int j = i + 1; j < timeStamps.size(); j++) {
                assertThat(timeStamps.get(i).getTimestampAsString().equals(timeStamps.get(j).getTimestampAsString()), is(false));
            }
        }
    }
}
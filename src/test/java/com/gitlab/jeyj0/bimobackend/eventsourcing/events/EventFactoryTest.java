package com.gitlab.jeyj0.bimobackend.eventsourcing.events;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class EventFactoryTest {

    private EventFactory eventFactory = new EventFactory();

    @Test
    public void shouldCreateSameEventWithTimestampSet() throws ClassNotFoundException {
        // given
        Event<EntityForTest> givenEvent = new Event<>(EventType.CREATED, EntityForTest.class, "this is some data");

        // when
        Event createdEvent = eventFactory.createEventWithCurrentTimestamp(givenEvent);

        // then
        assertThat(createdEvent.getId(), is(givenEvent.getId()));
        assertThat(createdEvent.getTimeStampFired(), notNullValue());
        assertThat(createdEvent.getClazz(), is(givenEvent.getClazz()));
        assertThat(createdEvent.getEventType(), is(givenEvent.getEventType()));
        assertThat(createdEvent.getData(), is(givenEvent.getData()));
    }

    private class EntityForTest extends DBEntity {

        public EntityForTest(String id) {
            super(id);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof EntityForTest &&
                    ((EntityForTest) obj).getId().equals(getId());
        }
    }
}
package com.gitlab.jeyj0.bimobackend.eventsourcing;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DBEntityTest {

    @Test
    public void shouldReturnIdHashCodeAsHashCode() {
        // given
        String givenId = "id";
        DBEntity entity = new DBEntityForTest(givenId);

        // when
        int hashCode = entity.hashCode();

        // then
        assertThat(hashCode, is(givenId.hashCode()));
    }

    @Test
    public void shouldReturnId() {
        // given
        String givenId = "id";
        DBEntity entity = new DBEntityForTest(givenId);

        // when
        String idAsReturned = entity.getId();

        // then
        assertThat(idAsReturned, is(givenId));
    }

    private class DBEntityForTest extends DBEntity {
        private DBEntityForTest(String id) {
            super(id);
        }

        @Override
        public boolean equals(Object obj) {
            return false;
        }
    }
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import org.junit.Test;

import static com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventType.DELETED;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DeletedEventFactoryTest {

    private DeletedEventFactory deletedEventFactory = new DeletedEventFactory();

    @Test
    public void shouldCreateDeletedEvent() {
        // given
        String entityAsString = "{\"id\":\"entityId\"}";
        DeletedEvent<EntityForTest> expectedDeletedEvent = new DeletedEvent<>(EntityForTest.class, entityAsString);

        // when
        DeletedEvent<EntityForTest> actualDeletedEvent = deletedEventFactory.createDeletedEvent(EntityForTest.class, entityAsString);

        // then
        assertThat(actualDeletedEvent.getClazz(), is(expectedDeletedEvent.getClazz()));
        assertThat(actualDeletedEvent.getEventType(), is(DELETED));
        assertThat(actualDeletedEvent.getData(), is(expectedDeletedEvent.getData()));
    }

    @Test
    public void shouldCreateDeletedEventByEntityAndObjectStringConverter() throws JsonProcessingException {
        // given
        EntityForTest entity = new EntityForTest("id");
        ObjectToStringConverterForTest objectToStringConverter = new ObjectToStringConverterForTest();
        String entityAsString = objectToStringConverter.convertObjectToString(entity);
        DeletedEvent<EntityForTest> expectedDeletedEvent = new DeletedEvent<>(EntityForTest.class, entityAsString);

        // when
        DeletedEvent<EntityForTest> actualDeletedEvent = deletedEventFactory.createDeletedEventByEntityAndObjectStringConverter(entity, objectToStringConverter);

        // then
        assertThat(actualDeletedEvent.getClazz(), is(expectedDeletedEvent.getClazz()));
        assertThat(actualDeletedEvent.getEventType(), is(DELETED));
        assertThat(actualDeletedEvent.getData(), is(expectedDeletedEvent.getData()));
    }

    private class EntityForTest extends DBEntity {

        public EntityForTest(String id) {
            super(id);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof EntityForTest &&
                    ((EntityForTest) obj).getId().equals(getId());
        }
    }

    private class ObjectToStringConverterForTest implements ObjectToStringConverter<EntityForTest> {

        @Override
        public String convertObjectToString(EntityForTest entity) {
            return "{\"id\":\"" + entity.getId() + "\"}";
        }
    }
}

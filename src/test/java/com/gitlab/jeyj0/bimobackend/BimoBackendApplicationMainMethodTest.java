package com.gitlab.jeyj0.bimobackend;

import com.gitlab.jeyj0.bimobackend.eventsourcing.events.Event;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRegistry;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BimoBackendApplicationMainMethodTest {

    @InjectMocks
    private BimoBackendApplication bimoBackendApplication;

    @Mock
    private EventRepository eventRepository;

    @Mock
    private EventRegistry eventRegistry;

    @Test
    @Ignore
    public void shouldStartMainMethod() {
        BimoBackendApplication.main(new String[]{});
    }

    @Test
    public void shouldReplayEventsFromDb() throws IOException, ClassNotFoundException {
        // given
        Event event1 = new Event();
        Event event2 = new Event();
        when(eventRepository.findAll(any(Sort.class))).thenReturn(List.of(event1, event2));

        // when
        bimoBackendApplication.run();

        // then
        verify(eventRegistry).replayEvent(event1);
        verify(eventRegistry).replayEvent(event2);
        verify(eventRegistry, times(2)).replayEvent(any(Event.class));
    }
}

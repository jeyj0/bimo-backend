package com.gitlab.jeyj0.bimobackend;

import com.gitlab.jeyj0.bimobackend.eventsourcing.events.Event;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRegistry;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.util.List;

@SpringBootApplication
public class BimoBackendApplication implements CommandLineRunner {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventRegistry eventRegistry;

    public static void main(String[] args) {
        SpringApplication.run(BimoBackendApplication.class, args);
    }

    @Override
    public void run(String... args) throws IOException, ClassNotFoundException {
        Sort sort = new Sort(Sort.Direction.ASC, "timeStampFired");
        List<Event> events = eventRepository.findAll(sort);
        for (Event event : events) {
            eventRegistry.replayEvent(event);
        }
    }
}

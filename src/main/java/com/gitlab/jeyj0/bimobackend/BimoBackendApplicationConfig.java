package com.gitlab.jeyj0.bimobackend;

import com.gitlab.jeyj0.bimobackend.domain.children.Child;
import com.gitlab.jeyj0.bimobackend.domain.status.Status;
import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BimoBackendApplicationConfig {

    @Bean
    public GenericEventSourcingBasedRepository<Child> genericEventSourcingBasedRepositoryForChild() {
        return new GenericEventSourcingBasedRepository<>();
    }

    @Bean
    public GenericEventSourcingBasedRepository<Status> genericEventSourcingBasedRepositoryForStatus() {
        return new GenericEventSourcingBasedRepository<>();
    }
}

package com.gitlab.jeyj0.bimobackend.graphql;

import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/**
 * Exists as to mark public methods that appear not-used as used (for IntelliJ as well as the developer).
 * Supposed to be used for GraphQL resolver methods.
 */
@Target(METHOD)
public @interface GraphQlResolver {
}

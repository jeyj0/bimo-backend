package com.gitlab.jeyj0.bimobackend.eventsourcing;

import java.util.UUID;

public class IDGenerator {

    public static String generateNewId() {
        return System.nanoTime() + "-" + UUID.randomUUID().toString();
    }
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions;

import java.io.IOException;

public class DuplicateIdException extends IOException {

    public DuplicateIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateIdException(String message) {
        this(message, null);
    }
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import org.springframework.stereotype.Service;

@Service
public class UpdatedEventFactory {

    public <ENTITY extends DBEntity> UpdatedEvent<ENTITY> createUpdatedEvent(Class<? extends DBEntity> entityType, String currentEntity, String updatedEntity) {
        String data = "{\"current\":" + currentEntity + ",\"updated\":" + updatedEntity + "}";
        return new UpdatedEvent<>((Class<ENTITY>) entityType, data);
    }

    public <ENTITY extends DBEntity> UpdatedEvent<ENTITY> createUpdatedEventByEntitiesAndObjectStringConverter(ENTITY currentEntity, ENTITY updatedEntity, ObjectToStringConverter<ENTITY> objectToStringConverter) throws JsonProcessingException {
        Class<? extends DBEntity> entityClass = currentEntity.getClass();
        String currentEntityAsString = objectToStringConverter.convertObjectToString(currentEntity);
        String updatedEntityAsString = objectToStringConverter.convertObjectToString(updatedEntity);
        return createUpdatedEvent(entityClass, currentEntityAsString, updatedEntityAsString);
    }
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions;

import java.io.IOException;

public class EntityNotFoundException extends IOException {

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotFoundException(String message) {
        this(message, null);
    }
}

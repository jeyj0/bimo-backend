package com.gitlab.jeyj0.bimobackend.eventsourcing.events;

public enum EventType {
    DELETED, UPDATED, CREATED
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;

public interface ObjectToStringConverter<ENTITY extends DBEntity> {

    String convertObjectToString(ENTITY entity) throws JsonProcessingException;
}

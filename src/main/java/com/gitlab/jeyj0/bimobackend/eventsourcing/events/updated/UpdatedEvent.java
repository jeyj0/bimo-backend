package com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.Event;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventType;

public class UpdatedEvent<ENTITY_TYPE extends DBEntity> extends Event<ENTITY_TYPE> {

    protected UpdatedEvent(Class<ENTITY_TYPE> entityType, String data) {
        super(EventType.UPDATED, entityType, data);
    }
}

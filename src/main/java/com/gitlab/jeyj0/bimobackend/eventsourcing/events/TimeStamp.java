package com.gitlab.jeyj0.bimobackend.eventsourcing.events;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TimeStamp {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
    private static String lastTimeAsString;
    private static int internalCounter = 0;

    private String timestampAsString;
    private int internalCounterAppendix;

    public String getTimestampAsString() {
        return timestampAsString + String.format("%03d", internalCounterAppendix);
    }

    public static TimeStamp getCurrent() {
        String currentTimeAsString = LocalDateTime.now().format(DATE_TIME_FORMATTER);

        if (currentTimeAsString.equals(lastTimeAsString)) {
            internalCounter++;
        } else {
            internalCounter = 0;
        }

        lastTimeAsString = currentTimeAsString;
        return new TimeStamp(currentTimeAsString, internalCounter);
    }
}

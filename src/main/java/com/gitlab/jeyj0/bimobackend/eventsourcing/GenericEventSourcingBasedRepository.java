package com.gitlab.jeyj0.bimobackend.eventsourcing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.StringToObjectConverter;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.Event;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventRegistry;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.created.CreatedEvent;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.created.CreatedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted.DeletedEvent;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted.DeletedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated.UpdatedEvent;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.updated.UpdatedEventFactory;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Repository
public class GenericEventSourcingBasedRepository<ENTITY extends DBEntity> {

    @Autowired
    private EventRegistry eventRegistry;

    @Autowired
    private CreatedEventFactory createdEventFactory;

    @Autowired
    private UpdatedEventFactory updatedEventFactory;

    @Autowired
    private DeletedEventFactory deletedEventFactory;

    @Autowired
    private ApplicationContext applicationContext;

    private HashSet<ENTITY> entities;

    public GenericEventSourcingBasedRepository() {
        this.entities = Sets.newHashSet();
    }

    public Set<ENTITY> getAllEntities() {
        return entities;
    }

    public void createEntity(ENTITY entity, ObjectToStringConverter<ENTITY> objectToStringConverter) throws DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        addEntity(entity, String.format("Cannot add an entity with the same id (%s).", entity.getId()));
        CreatedEvent<ENTITY> createdEvent = createdEventFactory.createCreatedEventByEntityAndObjectStringConverter(entity, objectToStringConverter);
        eventRegistry.fireEvent(createdEvent);
    }

    public void updateEntity(ENTITY currentEntity, ENTITY updatedEntity, ObjectToStringConverter<ENTITY> objectToStringConverter) throws DuplicateIdException, EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        removeEntity(currentEntity, String.format("Cannot update non-existing entity with id: %s", currentEntity.getId()));
        addEntity(updatedEntity, String.format("Cannot re-add entity with the same id (%s) as one that's already added.", updatedEntity.getId()));
        UpdatedEvent<ENTITY> updatedEvent = updatedEventFactory.createUpdatedEventByEntitiesAndObjectStringConverter(currentEntity, updatedEntity, objectToStringConverter);
        eventRegistry.fireEvent(updatedEvent);
    }

    public void deleteEntity(ENTITY entity, ObjectToStringConverter<ENTITY> objectToStringConverter) throws EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        removeEntity(entity, String.format("Cannot delete non-existing entity with id: %s", entity.getId()));
        DeletedEvent<ENTITY> deletedEvent = deletedEventFactory.createDeletedEventByEntityAndObjectStringConverter(entity, objectToStringConverter);
        eventRegistry.fireEvent(deletedEvent);
    }

    public void replayEvent(Event event) throws ClassNotFoundException, IOException {
        Class<?> entityClass = Class.forName(event.getClazz());
        StringToObjectConverter<ENTITY> stringToObjectConverter = getStringToObjectConverter(entityClass);

        switch (event.getEventType()) {
            case CREATED:
                ENTITY createdEntity = stringToObjectConverter.convertStringToObject(event.getData());
                addEntity(createdEntity, String.format("Cannot add an entity with the same id (%s).", createdEntity.getId()));
                break;
            case UPDATED:
                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode jsonNode = objectMapper.readTree(event.getData());
                String currentEntityAsString = jsonNode.get("current").toString();
                String updatedEntityAsString = jsonNode.get("updated").toString();
                ENTITY currentEntity = stringToObjectConverter.convertStringToObject(currentEntityAsString);
                ENTITY updatedEntity = stringToObjectConverter.convertStringToObject(updatedEntityAsString);
                removeEntity(currentEntity, "");
                addEntity(updatedEntity, "");
                break;
            case DELETED:
                ENTITY deletedEntity = stringToObjectConverter.convertStringToObject(event.getData());
                removeEntity(deletedEntity, "");
                break;
        }
    }

    private StringToObjectConverter<ENTITY> getStringToObjectConverter(Class<?> entityClass) {
        String[] beanNamesForType = applicationContext.getBeanNamesForType(ResolvableType.forClassWithGenerics(StringToObjectConverter.class, entityClass));
        return (StringToObjectConverter<ENTITY>) applicationContext.getBean(beanNamesForType[0]);
    }

    private boolean hasEntityWithId(String id) {
        for (ENTITY entity : entities) {
            if (entity.getId().equals(id))
                return true;
        }
        return false;
    }

    private void addEntity(ENTITY entity, String message) throws DuplicateIdException {
        if (hasEntityWithId(entity.getId()))
            throw new DuplicateIdException(message);
        entities.add(entity);
    }

    private void removeEntity(ENTITY entity, String message) throws EntityNotFoundException {
        boolean hasSuccessfullyRemovedEntity = entities.remove(entity);
        if (!hasSuccessfullyRemovedEntity)
            throw new EntityNotFoundException(message);
    }
}

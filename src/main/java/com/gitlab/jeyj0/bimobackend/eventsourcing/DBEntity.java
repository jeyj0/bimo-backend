package com.gitlab.jeyj0.bimobackend.eventsourcing;

import lombok.Getter;

public abstract class DBEntity {

    @Getter
    private final String id;

    public DBEntity(String id) {
        this.id = id;
    }

    @Override
    public abstract boolean equals(Object obj);

    @Override
    public final int hashCode() {
        return id.hashCode();
    }
}

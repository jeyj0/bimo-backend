package com.gitlab.jeyj0.bimobackend.eventsourcing.events.created;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.Event;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventType;

public class CreatedEvent<ENTITY_TYPE extends DBEntity> extends Event {

    protected CreatedEvent(Class<ENTITY_TYPE> entityType, String entityAsString) {
        super(EventType.CREATED, entityType, entityAsString);
    }
}

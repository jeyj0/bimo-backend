package com.gitlab.jeyj0.bimobackend.eventsourcing.events;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

public class Event<ENTITY_TYPE extends DBEntity> {

    @Id
    @Getter
    private String id;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String timeStampFired;
    @Getter
    private EventType eventType;
    @Getter
    private String clazz;
    @Getter
    private String data;

    public Event() {
    }

    public Event(EventType eventType, Class<ENTITY_TYPE> entityType, String data) {
        this.eventType = eventType;
        this.clazz = entityType.getName();
        this.data = data;
    }
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.converters;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;

import java.io.IOException;

public interface StringToObjectConverter<ENTITY extends DBEntity> {

    ENTITY convertStringToObject(String entityAsString) throws IOException;
}

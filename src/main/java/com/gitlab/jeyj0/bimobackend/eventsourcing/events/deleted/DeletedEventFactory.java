package com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import org.springframework.stereotype.Service;

@Service
public class DeletedEventFactory {

    public <ENTITY extends DBEntity> DeletedEvent<ENTITY> createDeletedEvent(Class<ENTITY> entityType, String entityAsString) {
        return new DeletedEvent<>(entityType, entityAsString);
    }

    public <ENTITY extends DBEntity> DeletedEvent<ENTITY> createDeletedEventByEntityAndObjectStringConverter(ENTITY entity, ObjectToStringConverter<ENTITY> objectToStringConverter) throws JsonProcessingException {
        Class<? extends DBEntity> entityClass = entity.getClass();
        String entityAsString = objectToStringConverter.convertObjectToString(entity);
        return createDeletedEvent((Class<ENTITY>) entityClass, entityAsString);
    }
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.events.deleted;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.Event;
import com.gitlab.jeyj0.bimobackend.eventsourcing.events.EventType;

public class DeletedEvent<ENTITY_TYPE extends DBEntity> extends Event {

    protected DeletedEvent(Class<ENTITY_TYPE> entityType, String entityAsString) {
        super(EventType.DELETED, entityType, entityAsString);
    }
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.events;

import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EventRegistry {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventFactory eventFactory;

    @Autowired
    private ApplicationContext applicationContext;

    public void fireEvent(Event event) throws ClassNotFoundException {
        event = eventFactory.createEventWithCurrentTimestamp(event);
        eventRepository.save(event);
    }

    public void replayEvent(Event event) throws ClassNotFoundException, IOException {
        Class<?> entityType = Class.forName(event.getClazz());
        GenericEventSourcingBasedRepository genericEventSourcingBasedRepository = getGenericEventSourcingBasedRepository(entityType);
        genericEventSourcingBasedRepository.replayEvent(event);
    }

    private GenericEventSourcingBasedRepository getGenericEventSourcingBasedRepository(Class entityType) {
        String genericEventSourcingBasedRepositoryBeanName = "genericEventSourcingBasedRepositoryFor" + entityType.getSimpleName();
        return (GenericEventSourcingBasedRepository) applicationContext.getBean(genericEventSourcingBasedRepositoryBeanName);
    }
}

package com.gitlab.jeyj0.bimobackend.eventsourcing.events.created;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import org.springframework.stereotype.Service;

@Service
public class CreatedEventFactory {

    public <ENTITY extends DBEntity> CreatedEvent<ENTITY> createCreatedEvent(Class<ENTITY> entityType, String entityAsString) {
        return new CreatedEvent<>(entityType, entityAsString);
    }

    public <ENTITY extends DBEntity> CreatedEvent<ENTITY> createCreatedEventByEntityAndObjectStringConverter(ENTITY entity, ObjectToStringConverter<ENTITY> objectToStringConverter) throws JsonProcessingException {
        Class<? extends DBEntity> entityClass = entity.getClass();
        String entityAsString = objectToStringConverter.convertObjectToString(entity);
        return createCreatedEvent((Class<ENTITY>) entityClass, entityAsString);
    }
}

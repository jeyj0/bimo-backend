package com.gitlab.jeyj0.bimobackend.eventsourcing.events;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import org.springframework.stereotype.Component;

@Component
public class EventFactory {

    public <ENTITY_TYPE extends DBEntity> Event<ENTITY_TYPE> createEventWithCurrentTimestamp(Event<ENTITY_TYPE> event) throws ClassNotFoundException {
        Event<ENTITY_TYPE> createdEvent = new Event<>(event.getEventType(), (Class<ENTITY_TYPE>) Class.forName(event.getClazz()), event.getData());
        createdEvent.setTimeStampFired(TimeStamp.getCurrent().getTimestampAsString());
        return createdEvent;
    }
}

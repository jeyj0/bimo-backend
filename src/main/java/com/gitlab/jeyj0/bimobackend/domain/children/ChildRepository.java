package com.gitlab.jeyj0.bimobackend.domain.children;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public class ChildRepository {

    @Autowired
    private GenericEventSourcingBasedRepository<Child> genericEventSourcingBasedRepository;

    @Autowired
    private ChildFactory childFactory;

    @Autowired
    private ChildObjectToStringConverter childObjectToStringConverter;

    public Set<Child> getAllChildren() {
        return genericEventSourcingBasedRepository.getAllEntities();
    }

    public Child findChildById(String id) throws EntityNotFoundException {
        for (Child child : getAllChildren()) {
            if (child.getId().equals(id))
                return child;
        }
        throw new EntityNotFoundException("Could not find child with id " + id);
    }

    public Child createChild(String name) throws DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        Child child = childFactory.createChildWithoutIdAndStatuses(name);
        genericEventSourcingBasedRepository.createEntity(child, childObjectToStringConverter);
        return child;
    }

    public Child updateChild(String id, String name) throws EntityNotFoundException, JsonProcessingException, DuplicateIdException, ClassNotFoundException {
        Child currentChild = findChildById(id);
        Child updatedChild = childFactory.createChild(id, name, currentChild.getStatusIds());
        genericEventSourcingBasedRepository.updateEntity(currentChild, updatedChild, childObjectToStringConverter);
        return updatedChild;
    }

    public boolean deleteChild(String childId) throws EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        Optional<Child> childToDelete = getAllChildren().stream().filter(child -> child.getId().equals(childId)).findFirst();
        if (!childToDelete.isPresent())
            throw new EntityNotFoundException(String.format("Child with id (%s) cannot be found and therefor not deleted.", childId));
        genericEventSourcingBasedRepository.deleteEntity(childToDelete.get(), childObjectToStringConverter);
        return true;
    }

    public Child addStatusToChild(String childId, String statusId) throws EntityNotFoundException, ClassNotFoundException, DuplicateIdException, JsonProcessingException {
        Child currentChild = findChildById(childId);
        Set<String> statusIds = currentChild.getStatusIds();
        statusIds.add(statusId);
        Child updatedChild = childFactory.createChild(currentChild.getId(), currentChild.getName(), statusIds);
        genericEventSourcingBasedRepository.updateEntity(currentChild, updatedChild, childObjectToStringConverter);
        return updatedChild;
    }

    public Child removeStatusFromChild(String childId, String statusId) throws EntityNotFoundException, ClassNotFoundException, DuplicateIdException, JsonProcessingException {
        Child currentChild = findChildById(childId);
        Set<String> statusIds = currentChild.getStatusIds();
        statusIds.remove(statusId);
        Child updatedChild = childFactory.createChild(currentChild.getId(), currentChild.getName(), statusIds);
        genericEventSourcingBasedRepository.updateEntity(currentChild, updatedChild, childObjectToStringConverter);
        return updatedChild;
    }
}

package com.gitlab.jeyj0.bimobackend.domain.status;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import com.gitlab.jeyj0.bimobackend.graphql.GraphQlResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class StatusMutation implements GraphQLMutationResolver {

    @Autowired
    private StatusRepository statusRepository;

    @GraphQlResolver
    public Status createStatus(String name) throws JsonProcessingException, DuplicateIdException, ClassNotFoundException {
        Objects.requireNonNull(name);
        return statusRepository.createStatus(name);
    }

    @GraphQlResolver
    public Status renameStatus(String id, String name) throws ClassNotFoundException, DuplicateIdException, EntityNotFoundException, JsonProcessingException {
        Objects.requireNonNull(id);
        Objects.requireNonNull(name);
        return statusRepository.updateStatus(id, name);
    }

    @GraphQlResolver
    public boolean deleteStatus(String id) throws EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        Objects.requireNonNull(id);
        return statusRepository.deleteStatus(id);
    }
}

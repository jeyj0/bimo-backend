package com.gitlab.jeyj0.bimobackend.domain.status;

import org.springframework.stereotype.Service;

import static com.gitlab.jeyj0.bimobackend.eventsourcing.IDGenerator.generateNewId;

@Service
public class StatusFactory {

    public Status createStatus(String id, String name) {
        if (id == null)
            id = generateNewId();
        return new Status(id, name);
    }

    public Status createStatusWithoutId(String name) {
        return createStatus(null, name);
    }
}

package com.gitlab.jeyj0.bimobackend.domain.children;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.gitlab.jeyj0.bimobackend.domain.status.Status;
import com.gitlab.jeyj0.bimobackend.domain.status.StatusRepository;
import com.gitlab.jeyj0.bimobackend.graphql.GraphQlResolver;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ChildResolver implements GraphQLResolver<Child> {

    @Autowired
    private StatusRepository statusRepository;

    @GraphQlResolver
    public List<Status> statuses(Child child) {
        Set<Status> statusSet = statusRepository.findStatusesByIds(child.getStatusIds());
        return Lists.newArrayList(statusSet);
    }
}

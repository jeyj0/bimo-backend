package com.gitlab.jeyj0.bimobackend.domain.children;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import lombok.Value;

import java.util.Objects;
import java.util.Set;

@Value
public class Child extends DBEntity {

    String name;
    Set<String> statusIds;

    Child(String id, String name, Set<String> statusIds) {
        super(id);
        this.name = name;
        this.statusIds = statusIds;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj != null) &&
                (obj instanceof Child) &&
                Objects.equals(((Child) obj).getId(), this.getId()) &&
                Objects.equals(((Child) obj).getName(), this.getName()) &&
                Objects.equals(((Child) obj).getStatusIds(), this.getStatusIds());
    }
}

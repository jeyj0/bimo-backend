package com.gitlab.jeyj0.bimobackend.domain.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.GenericEventSourcingBasedRepository;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public class StatusRepository {

    @Autowired
    private GenericEventSourcingBasedRepository<Status> genericEventSourcingBasedRepository;

    @Autowired
    private StatusFactory statusFactory;

    @Autowired
    private StatusObjectToStringConverter statusObjectToStringConverter;

    public Set<Status> getAllStatus() {
        return genericEventSourcingBasedRepository.getAllEntities();
    }

    public Set<Status> findStatusesByIds(Set<String> statusIds) {
        Set<Status> statuses = Sets.newHashSet();
        Set<String> remainingStatusIds = Sets.newHashSet(statusIds);

        for (Status status : getAllStatus()) {
            if (remainingStatusIds.contains(status.getId())) {
                statuses.add(status);
                remainingStatusIds.remove(status.getId());
            }
        }

        return statuses;
    }

    public Status findStatusById(String id) {
        return findStatusesByIds(Set.of(id)).iterator().next();
    }

    public Status createStatus(String name) throws ClassNotFoundException, DuplicateIdException, JsonProcessingException {
        Status status = statusFactory.createStatusWithoutId(name);
        genericEventSourcingBasedRepository.createEntity(status, statusObjectToStringConverter);
        return status;
    }

    public Status updateStatus(String id, String name) throws JsonProcessingException, ClassNotFoundException, DuplicateIdException, EntityNotFoundException {
        Status currentStatus = findStatusById(id);
        Status updatedStatus = statusFactory.createStatus(currentStatus.getId(), name);
        genericEventSourcingBasedRepository.updateEntity(currentStatus, updatedStatus, statusObjectToStringConverter);
        return updatedStatus;
    }

    public boolean deleteStatus(String id) throws EntityNotFoundException, ClassNotFoundException, JsonProcessingException {
        Status status = findStatusById(id);
        genericEventSourcingBasedRepository.deleteEntity(status, statusObjectToStringConverter);
        return true;
    }
}

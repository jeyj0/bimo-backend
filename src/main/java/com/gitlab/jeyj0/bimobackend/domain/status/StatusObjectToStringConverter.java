package com.gitlab.jeyj0.bimobackend.domain.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import org.springframework.stereotype.Component;

@Component
public class StatusObjectToStringConverter implements ObjectToStringConverter<Status> {

    @Override
    public String convertObjectToString(Status status) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(status);
    }
}

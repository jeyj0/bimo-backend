package com.gitlab.jeyj0.bimobackend.domain.status;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.StringToObjectConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class StringToStatusObjectConverter implements StringToObjectConverter<Status> {

    @Autowired
    private StatusFactory statusFactory;

    @Override
    public Status convertStringToObject(String statusAsString) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(statusAsString);
        return statusFactory.createStatus(
                jsonNode.get("id").asText(),
                jsonNode.get("name").asText()
        );
    }
}

package com.gitlab.jeyj0.bimobackend.domain.children;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.StringToObjectConverter;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Set;

@Component
public class StringToChildObjectConverter implements StringToObjectConverter<Child> {

    @Autowired
    private ChildFactory childFactory;

    public Child convertStringToObject(String childAsString) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(childAsString);

        Set<String> statusIds = convertStatusIdsToSet(jsonNode.get("statusIds"));

        return childFactory.createChild(
                jsonNode.get("id").asText(),
                jsonNode.get("name").asText(),
                statusIds
        );
    }

    private Set<String> convertStatusIdsToSet(JsonNode jsonNode) {
        Set<String> statusIds = Sets.newHashSet();
        jsonNode.elements().forEachRemaining(status -> statusIds.add(status.asText()));
        return statusIds;
    }
}

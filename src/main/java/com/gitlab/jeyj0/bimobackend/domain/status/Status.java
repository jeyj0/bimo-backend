package com.gitlab.jeyj0.bimobackend.domain.status;

import com.gitlab.jeyj0.bimobackend.eventsourcing.DBEntity;
import lombok.Value;

import java.util.Objects;

@Value
public class Status extends DBEntity {

    String name;

    Status(String id, String name) {
        super(id);
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj != null) &&
                (obj instanceof Status) &&
                Objects.equals(((Status) obj).getId(), this.getId()) &&
                Objects.equals(((Status) obj).getName(), this.getName());
    }
}

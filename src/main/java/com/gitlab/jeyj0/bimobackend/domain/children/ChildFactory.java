package com.gitlab.jeyj0.bimobackend.domain.children;

import com.google.common.collect.Sets;
import org.springframework.stereotype.Service;

import java.util.Set;

import static com.gitlab.jeyj0.bimobackend.eventsourcing.IDGenerator.generateNewId;

@Service
public class ChildFactory {

    public Child createChild(String id, String name, Set<String> statusIds) {
        if (id == null) {
            id = generateNewId();
        }
        if (statusIds == null) {
            statusIds = Sets.newHashSet();
        }
        return new Child(id, name, statusIds);
    }

    public Child createChildWithoutIdAndStatuses(String name) {
        return createChild(null, name, null);
    }
}

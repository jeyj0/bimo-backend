package com.gitlab.jeyj0.bimobackend.domain.children;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.DuplicateIdException;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import com.gitlab.jeyj0.bimobackend.graphql.GraphQlResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ChildMutation implements GraphQLMutationResolver {

    @Autowired
    private ChildRepository childRepository;

    @GraphQlResolver
    public Child createChild(String name) throws DuplicateIdException, JsonProcessingException, ClassNotFoundException {
        Objects.requireNonNull(name);
        return childRepository.createChild(name);
    }

    @GraphQlResolver
    public Child renameChild(String id, String name) throws EntityNotFoundException, JsonProcessingException, DuplicateIdException, ClassNotFoundException {
        Objects.requireNonNull(id);
        Objects.requireNonNull(name);
        return childRepository.updateChild(id, name);
    }

    @GraphQlResolver
    public boolean deleteChild(String id) throws EntityNotFoundException, JsonProcessingException, ClassNotFoundException {
        Objects.requireNonNull(id);
        return childRepository.deleteChild(id);
    }

    @GraphQlResolver
    public Child addStatusToChild(String childId, String statusId) throws ClassNotFoundException, JsonProcessingException, DuplicateIdException, EntityNotFoundException {
        Objects.requireNonNull(childId);
        Objects.requireNonNull(statusId);
        return childRepository.addStatusToChild(childId, statusId);
    }

    @GraphQlResolver
    public Child removeStatusFromChild(String childId, String statusId) throws ClassNotFoundException, JsonProcessingException, DuplicateIdException, EntityNotFoundException {
        Objects.requireNonNull(childId);
        Objects.requireNonNull(statusId);
        return childRepository.removeStatusFromChild(childId, statusId);
    }
}

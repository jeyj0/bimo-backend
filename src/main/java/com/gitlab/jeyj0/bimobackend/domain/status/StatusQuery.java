package com.gitlab.jeyj0.bimobackend.domain.status;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.gitlab.jeyj0.bimobackend.graphql.GraphQlResolver;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StatusQuery implements GraphQLQueryResolver {

    @Autowired
    private StatusRepository statusRepository;

    @GraphQlResolver
    public List<Status> allStatuses() {
        return Lists.newArrayList(statusRepository.getAllStatus());
    }
}

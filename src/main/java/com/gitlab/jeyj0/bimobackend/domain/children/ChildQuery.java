package com.gitlab.jeyj0.bimobackend.domain.children;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.gitlab.jeyj0.bimobackend.eventsourcing.exceptions.EntityNotFoundException;
import com.gitlab.jeyj0.bimobackend.graphql.GraphQlResolver;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChildQuery implements GraphQLQueryResolver {

    @Autowired
    private ChildRepository childRepository;

    @GraphQlResolver
    public List<Child> allChildren() {
        return Lists.newArrayList(childRepository.getAllChildren());
    }

    @GraphQlResolver
    public Child childById(String id) throws EntityNotFoundException {
        return childRepository.findChildById(id);
    }
}

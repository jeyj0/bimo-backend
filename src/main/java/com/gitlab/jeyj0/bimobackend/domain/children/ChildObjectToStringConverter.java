package com.gitlab.jeyj0.bimobackend.domain.children;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jeyj0.bimobackend.eventsourcing.converters.ObjectToStringConverter;
import org.springframework.stereotype.Component;

@Component
public class ChildObjectToStringConverter implements ObjectToStringConverter<Child> {

    public String convertObjectToString(Child child) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(child);
    }
}
